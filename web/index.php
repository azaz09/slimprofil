<?php
session_start();
error_reporting(-1);

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
$app = new \Slim\App;
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);


$container = $app->getContainer();

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../views', [
        'cache' => false
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

    
$app->get('/', function ($request, $response) {

    return $this->view->render($response, 'index.twig');
})->setName('index');

$app->post('/', function ($args) {
    $adress = 'nanquery@gmail.com'; //dokąd to pójdzie
       $topic = $POST['topic'];
       $content = $POST['content'];
       return mail($adress, $topic, $content);
            
})->setName('send');
$app->run();

